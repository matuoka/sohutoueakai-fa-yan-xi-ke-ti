Rails.application.routes.draw do
  
  resources :tweets
  root 'tweets#index'
  get 'tweets/:id/show',to:'tweets#show'
end
